package ch.sourcemotion.vertx.retry

import ch.sourcemotion.vertx.retry.logging.TestLogDelegateFactory
import com.nhaarman.mockitokotlin2.mockingDetails
import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import io.vertx.core.Vertx
import io.vertx.core.spi.logging.LogDelegate
import io.vertx.junit5.Checkpoint
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(VertxExtension::class)
class VertxRetryLoggingTest {

    companion object {
        private const val ADDR = "test-addr"
    }

    private val random = Random()

    /**
     * Test that the logger will called expect times
     */
    @Test
    internal fun traceLevel(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val logLevel = VertxRetryLogLevel.TRACE

        executeFailingAndVerifyLoggerCalls(logLevel, vertx, getNextRandomRetryCount(), context, checkpoint)
    }

    /**
     * Test that the logger will called expect times
     */
    @Test
    internal fun debugLevel(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val logLevel = VertxRetryLogLevel.DEBUG

        executeFailingAndVerifyLoggerCalls(logLevel, vertx, getNextRandomRetryCount(), context, checkpoint)
    }

    /**
     * Test that the logger will called expect times
     */
    @Test
    internal fun warnLevel(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val logLevel = VertxRetryLogLevel.WARN

        executeFailingAndVerifyLoggerCalls(logLevel, vertx, getNextRandomRetryCount(), context, checkpoint)
    }

    /**
     * Test that the logger will called expect times
     */
    @Test
    internal fun errorLevel(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val logLevel = VertxRetryLogLevel.ERROR

        executeFailingAndVerifyLoggerCalls(logLevel, vertx, getNextRandomRetryCount(), context, checkpoint)
    }

    /**
     * Test that the logger will called expect times
     */
    @Test
    internal fun infoLevel(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val logLevel = VertxRetryLogLevel.INFO

        executeFailingAndVerifyLoggerCalls(logLevel, vertx, getNextRandomRetryCount(), context, checkpoint)
    }

    private fun executeFailingAndVerifyLoggerCalls(
        logLevel: VertxRetryLogLevel,
        vertx: Vertx,
        retries: Int,
        context: VertxTestContext,
        checkpoint: Checkpoint
    ) {
        TestLogDelegateFactory.level = logLevel
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            this.retries = retries
            retryPause = 100
            failureLogLevel = logLevel
        })
        testAsync(context, checkpoint) {
            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR)
                }
            }

            val loggerOfInterest = TestLogDelegateFactory.loggersByName[VertxRetry::class.java.name]
            loggerOfInterest.shouldNotBeNull()
            countFailedLogMessageCount(loggerOfInterest, logLevel).shouldBe(retries)
        }
    }

    /**
     * Get all invocations of the [LogDelegate] mock and filter the expect level related function calls with the expected
     * log message.
     *
     * @return Count of the relevant function calls.
     */
    private fun countFailedLogMessageCount(logDelegate: LogDelegate, level: VertxRetryLogLevel): Int {
        val functionNamePart = level.name.toLowerCase()
        return mockingDetails(logDelegate).invocations.filter { it.location.toString().contains(functionNamePart) }
            .filter { it.rawArguments.any { arg -> arg is String && arg.contains("did fail on address") } }.count()
    }

    /**
     * Generator of random retry count, so not each test run is done with the same amount of retries.
     */
    private fun getNextRandomRetryCount(): Int {
        var retries = random.nextInt(11)
        while (retries < 1) {
            retries = random.nextInt(11)
        }
        return retries
    }
}