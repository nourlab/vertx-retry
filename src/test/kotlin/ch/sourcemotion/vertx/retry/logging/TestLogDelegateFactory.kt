package ch.sourcemotion.vertx.retry.logging

import ch.sourcemotion.vertx.retry.VertxRetryLogLevel
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import io.vertx.core.spi.logging.LogDelegate
import io.vertx.core.spi.logging.LogDelegateFactory

class TestLogDelegateFactory : LogDelegateFactory {
    companion object {
        val loggersByName = HashMap<String, LogDelegate>()
        var level = VertxRetryLogLevel.INFO
    }

    override fun createDelegate(name: String): LogDelegate {
        val logDelegate = loggersByName.getOrDefault(name, mock {
            on { isTraceEnabled } doAnswer { VertxRetryLogLevel.TRACE == level }
            on { isDebugEnabled } doAnswer { VertxRetryLogLevel.DEBUG == level || VertxRetryLogLevel.TRACE == level }
            on { isInfoEnabled } doAnswer { VertxRetryLogLevel.INFO == level || VertxRetryLogLevel.DEBUG == level || VertxRetryLogLevel.TRACE == level }
            on { isWarnEnabled } doAnswer { VertxRetryLogLevel.WARN == level || VertxRetryLogLevel.INFO == level || VertxRetryLogLevel.DEBUG == level || VertxRetryLogLevel.TRACE == level }
        })
        loggersByName[name] = logDelegate
        return logDelegate
    }
}