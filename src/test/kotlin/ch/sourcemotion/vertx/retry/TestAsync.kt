package ch.sourcemotion.vertx.retry

import io.vertx.junit5.Checkpoint
import io.vertx.junit5.VertxTestContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun testAsync(context: VertxTestContext, checkpoint: Checkpoint, block: suspend CoroutineScope.() -> Unit) {
    GlobalScope.launch {
        block(this)
    }.invokeOnCompletion {
        it?.let { context.failNow(it) } ?: checkpoint.flag()
    }
}