package ch.sourcemotion.vertx.retry

import io.vertx.core.eventbus.ReplyException
import io.vertx.core.eventbus.ReplyFailure

/**
 * Wrapper, replacement of [ReplyFailure] to represent other failures too.
 */
enum class VertxFailureCase {
    NO_HANDLERS,
    RECIPIENT_FAILURE,
    TIMEOUT,
    OTHER;

    companion object {
        internal fun forException(e: Exception): VertxFailureCase = if (e is ReplyException) {
            when (e.failureType()) {
                ReplyFailure.TIMEOUT -> TIMEOUT
                ReplyFailure.NO_HANDLERS -> NO_HANDLERS
                ReplyFailure.RECIPIENT_FAILURE -> RECIPIENT_FAILURE
                else -> OTHER
            }
        } else {
            OTHER
        }
    }
}