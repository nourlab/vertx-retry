package ch.sourcemotion.vertx.retry

enum class VertxRetryLogLevel {
    NONE,
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR
}