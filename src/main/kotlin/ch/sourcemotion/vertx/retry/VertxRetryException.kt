package ch.sourcemotion.vertx.retry

/**
 * Exception to normalize
 *
 * @param retryCases List of failure cases, after them retries did follow
 * @param finalCase The last failure case that did happen before this exception was thrown.
 */
class VertxRetryException private constructor(val retryCases: List<VertxFailureCase>, val finalCase: VertxFailureCase) :
    Exception() {
    companion object {
        internal fun build(retryCases: List<VertxFailureCase>, finalCase: VertxFailureCase): VertxRetryException {
            return VertxRetryException(retryCases, finalCase)
        }
    }
}