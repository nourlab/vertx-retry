# Vert.x retry

[![pipeline status](https://gitlab.com/michel.werren/vertx-retry/badges/master/pipeline.svg)](https://gitlab.com/michel.werren/vertx-retry/commits/master)
[![coverage report](https://gitlab.com/michel.werren/vertx-retry/badges/master/coverage.svg)](https://gitlab.com/michel.werren/vertx-retry/commits/master)

This library represents an even more lightweight alternative to the circuit breaker of Vert.x. In general it's an simple
retry mechanism for some configurable failure cases and retry count. Optionally the failed tries can be logged and in the case of failure
an specific exception will be thrown at the end if all retries did fail. That exception contains each exception that did happen during a try and the final / last exception.

This small library targets to cover e.g. situations where consumers are not available / reachable for a short time like in redeployment scenarios.
Specially when e.g. a database schema will change during a service update which is not backward compatible we usually 
have to redeploy all those service instances at once, so they get unavailable for few seconds. Such periods can get bridged 
with this library.

## Installation

This library is available under the Maven repository: [Jcenter](http://jcenter.bintray.com)

### Maven

```xml
<dependency>
  <groupId>ch.sourcemotion.vertx</groupId>
  <artifactId>vertx-retry</artifactId>
  <version>1.0.0</version>
</dependency>
```

### Gradle

```kotlin
implementation("ch.sourcemotion.vertx:vertx-retry:1.0.0")
```


## Usage

To get an instance of Vert.x retry you can simple call:
`ch.sourcemotion.vertx.retry.VertxRetry.create`

## Configuration
The create function is parameterizable with an optional instance of `ch.sourcemotion.vertx.retry.VertxRetryOptions`

### Default configuration
- retries: 5
- pause between each retry: 1000 millis
- failure cases to catch: all [NO_HANDLERS, RECIPIENT_FAILURE, TIMEOUT, OTHER] (all)
- logging: disabled 

## Make a call
This library is implemented in Kotlin, but has 100% interoperability with Java.

From Java you can just call a variant of `ch.sourcemotion.vertx.retry.VertxRetry#request` 
From Kotlin one of the Java variants as well or the one, specially for Kotlin `ch.sourcemotion.vertx.retry.VertxRetry#requestAwait`. This one is suspending.

### Exception / failures
If all retries did not success, finally an instance of `ch.sourcemotion.vertx.retry.VertxRetryException` will be thrown (Kotlin requestAwait)
or the AsyncResult / Promise finalized with. 

> If a failure happen which is no configured e.g. you configured Vert.x retry only to handle NO_HANDLERS failures and
> a TIMEOUT appears, the call will fast fail like the behavior of the original `io.vertx.core.eventbus.EventBus`.