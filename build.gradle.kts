import com.jfrog.bintray.gradle.BintrayExtension
import nu.studer.gradle.credentials.domain.CredentialsContainer
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.util.*

plugins {
    java
    `maven-publish`
    jacoco
    kotlin("jvm") version "1.3.50"
    id("com.jfrog.bintray") version "1.8.4"
    id("nu.studer.credentials") version "1.0.7"
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ksoichiro.console.reporter") version "0.6.2"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api(vertx("core"))
    api(vertx("lang-kotlin"))
    api(vertx("lang-kotlin-coroutines"))

    testCompile(junit5Jupiter("jupiter-api"))
    testCompile(junit5Jupiter("jupiter-engine"))
    testCompile(vertx("junit5"))
    testCompile("io.kotlintest:kotlintest-runner-junit5:3.4.1")
    testCompile(log4j("slf4j-impl"))
    testCompile(log4j("core"))
    testCompile(log4j("api"))
    testCompile("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
}

fun vertx(module: String) = "io.vertx:vertx-$module:3.8.1"
fun junit5Jupiter(module: String) = "org.junit.jupiter:junit-$module:5.5.1"
fun log4j(module: String) = "org.apache.logging.log4j:log4j-$module:2.12.1"

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

val credentials: CredentialsContainer by extra
var bintrayUser = credentials.propertyMissing("bintray_user")?.toString() ?: runCatching {
    project.property("bintray_user")?.toString()
}.getOrElse { "" }
var bintrayApiKey =
    credentials.propertyMissing("bintray_api_key")?.toString() ?: runCatching {
        project.property("bintray_api_key")?.toString()
    }.getOrElse { "" }

val publicationName = "vertxRetry"

bintray {
    user = bintrayUser
    key = bintrayApiKey
    setPublications(publicationName)

    pkg(closureOf<BintrayExtension.PackageConfig> {
        repo = "maven"
        name = "vertx-retry"
        userOrg = "michel-werren"
        vcsUrl = "https://gitlab.com/michel.werren/vertx-retry"
        version(closureOf<BintrayExtension.VersionConfig> {
            name = project.version.toString()
            released = Date().toString()
        })
        setLicenses("MIT")
    })
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        register(publicationName, MavenPublication::class.java) {
            from(components["java"])
            artifact(sourcesJar.get())
            pom {
                groupId = "ch.sourcemotion.vertx"
                artifactId = "vertx-retry"
                version = project.version.toString()
                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("http://www.opensource.org/licenses/MIT")
                        distribution.set("https://gitlab.com/michel.werren/vertx-retry")
                    }
                }
            }
        }
    }
}

jacoco {
    toolVersion = "0.8.4"
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    jacocoTestReport {
        val classTree = sourceSets.main.get().output.asFileTree.matching {
            // Exclude some Companion classes and default impls because Jacoco is unable to analyse them properly.
            exclude(
                "**/VertxRetry\$Companion.class",
                "**/VertxRetry\$DefaultImpls.class",
                "**/VertxFailureCase\$Companion.class",
                "**/VertxRetryException\$Companion.class"
            )
        }
        classDirectories.setFrom(classTree)

        reports {
            xml.isEnabled = true
            html.isEnabled = true
        }
    }

    withType<Test> {
        useJUnitPlatform()
        systemProperties["vertx.logger-delegate-factory-class-name"] =
            "ch.sourcemotion.vertx.retry.logging.TestLogDelegateFactory"
        finalizedBy(jacocoTestReport)
    }

    findByName("afterReleaseBuild")?.dependsOn(findByName("bintrayUpload"))
}